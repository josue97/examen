package com.example.examen;

import java.io.Serializable;

public class Rectangulo implements Serializable
{
    private String nombre;
    private int base;
    private int altura;


    public Rectangulo(String nombre,int base, int altura, int calcularArea, int calcularPerimetro)
    {
        this.nombre = nombre;
        this.base = base ;
        this.altura = altura;
    }

    public Rectangulo() {

    }

    public String getNombre() {
        return nombre;
    }

    public int getBase() {
        return base;
    }

    public int getAltura() {
        return altura;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int CalcularArea()
    {
        int Area = this.base * this.altura;

        return Area;
    }

    private int CalcularPerimetro()
    {
        int perimetro = 2 * (altura + base);

        return perimetro;
    }

}
